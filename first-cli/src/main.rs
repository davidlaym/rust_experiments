extern crate clap;
extern crate reqwest;
extern crate serde;

#[macro_use]
extern crate serde_derive;
extern crate serde_json;

#[macro_use]
extern crate prettytable;

#[macro_use]
extern crate simple_error;

use clap::App;
use simple_error::SimpleError;

#[derive(Debug, Deserialize, Serialize)]
struct Author {
  name: String,
  url: String,
}

#[derive(Debug, Deserialize, Serialize)]
struct FeedItem {
  id: String,
  title: String,
  content_text: String,
  url: String,
  date_published: String,
  author: Author,
}

#[derive(Debug, Deserialize, Serialize)]
struct Feed {
  version: String,
  title: String,
  home_page_url: String,
  feed_url: String,
  description: String,
  author: Author,
  items: Vec<FeedItem>,
}

fn main() {
  let matches = App::new("readrust")
    .version("0.1")
    .author("David Lay <davidlaym@gmail.com>")
    .about("Reads readrust.net")
    .args_from_usage(
      "-n, --number=[NUMBER] 'Only print the NUMBER most recent posts'
                      -c. --count 'Show de count of posts'",
    ).get_matches();

  let feed = match get_feed() {
    Ok(feed) => feed,
    Err(err) => {
      eprint!("Fatal error: {}", err);
      ::std::process::exit(1)
    }
  };

  if matches.is_present("count") {
    print_count(&feed);
  } else {
    let iter = feed.items.iter();

    if let Some(user_input) = matches.value_of("number") {
      let number = parse_or_default(user_input, 50);
      print_feed_table(iter.take(number))
    } else {
      print_feed_table(iter)
    }
  }
}

fn parse_or_default(input: &str, default: usize) -> usize {
  match input.parse::<usize>() {
    Ok(n) => n,
    Err(err) => parse_use_default(err, input, default),
  }
}

fn parse_use_default(err: std::num::ParseIntError, original_number: &str, default: usize) -> usize {
  eprintln!(
    "Can't parse number from input: \"{number}\" for the ammount of posts ({error})",
    number = original_number,
    error = err
  );
  println!("using default amount: {}", default);
  default
}

pub static URL: &str = "http://readrust.net/rust2018/feed.json";

fn get_feed() -> Result<Feed, SimpleError> {
  let client = reqwest::Client::new();
  let mut req = client.get(URL);
  let mut resp = req.send().unwrap();

  assert!(resp.status().is_success());

  let json = resp.text().unwrap();

  match serde_json::from_str(&json) {
    Ok(feed) => Ok(feed),
    Err(err) => bail!("Can't parse incoming feed {}", err),
  }
}

fn print_count(feed: &Feed) {
  println!("Number of posts: {}", feed.items.len());
}

fn print_feed_table<'feed, I: Iterator<Item = &'feed FeedItem>>(items: I) {
  let mut table = prettytable::Table::new();

  table.add_row(row!["Title", "Author", "Link"]);

  for item in items {
    let title = if item.title.len() >= 50 {
      &item.title[0..49]
    } else {
      &item.title
    };

    table.add_row(row![title, item.author.name, item.url]);
  }

  table.printstd();
}
